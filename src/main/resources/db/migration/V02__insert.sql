INSERT INTO mw_categoria(
	id, version, criado_em, nome)
	VALUES ('6db65943-4cbd-41b7-a47b-b470797da997', 0, CURRENT_DATE, 'Musica');




INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('bf4a0275-b4fe-4114-9e1e-07140c0f653b', 0, CURRENT_DATE, 'Axe', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('2c8c7e36-1f3f-4220-8a8b-cac1c12bf267', 0, CURRENT_DATE, 'Mpb', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('daaaaee5-84b0-4623-96f8-c001b3629eb9', 0, CURRENT_DATE, 'Pop', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('9b116097-d4a0-4b09-814d-2db6e8b8af35', 0, CURRENT_DATE, 'Blues', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('8ca3f99e-13a6-4a8d-a216-7906425598b6', 0, CURRENT_DATE, 'Bossa', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('75424efb-7ae8-4898-8a84-aa8e4937a359', 0, CURRENT_DATE, 'Eletronica', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('9c06f1ed-8807-4167-a24f-4d1cec6683c5', 0, CURRENT_DATE, 'Forro', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('f5f837aa-e770-4f9e-86b1-9149d0b5e737', 0, CURRENT_DATE, 'Funk', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('02e5237c-f41d-4b93-b05d-c14dea4c8e0e', 0, CURRENT_DATE, 'Funk_Carioca', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('b1433ce0-4ad6-475e-8a31-8d1ad75f2f1b', 0, CURRENT_DATE, 'Golpel', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('4999987d-a72c-4bfe-a084-b26cb888312b', 0, CURRENT_DATE, 'Hip_Hop', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('d3b64e43-a48f-4380-92c9-66c8f67556bf', 0, CURRENT_DATE, 'Jazz', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('a9e98ee9-10d2-45b5-8c1e-54ab8623902d', 0, CURRENT_DATE, 'Kizomba', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('7cfb0e03-00b1-42c0-9b71-2b281e11b0c1', 0, CURRENT_DATE, 'Pagode', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('32800125-9aca-445c-be88-be244d4db2d8', 0, CURRENT_DATE, 'Pop_Rock', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('7c3efdd8-3a22-4191-a09e-a432d7c928da', 0, CURRENT_DATE, 'ReB', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('0e814fbd-6eeb-4ac8-a71b-c76e83d116f1', 0, CURRENT_DATE, 'Rap', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('b43d114c-99af-4d92-96dd-7ca6df636d82', 0, CURRENT_DATE, 'Reggae', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('3d385112-d176-4d49-8e26-d3fa5e40a508', 0, CURRENT_DATE, 'Reggaeton', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('d1d55651-56d9-4714-a757-ba31ed7490cc', 0, CURRENT_DATE, 'Rock', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('5d5492d4-9a79-4582-bbe0-4ff16c1f0962', 0, CURRENT_DATE, 'Samba', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('8ce16eea-cca3-48dc-a501-d9bed80073dd', 0, CURRENT_DATE, 'Sertanejo', '6db65943-4cbd-41b7-a47b-b470797da997');

INSERT INTO mw_genero(
	id, version, criado_em, nome, categoria_id)
	VALUES ('3c22bcd8-624e-438b-b546-fbbce97894de', 0, CURRENT_DATE, 'Soul', '6db65943-4cbd-41b7-a47b-b470797da997');




INSERT INTO mw_cliente(
	id, version, celular, email, dt_nascimento, nome)
	VALUES ('0be8e982-b356-4a68-bc05-8b9fc7bb1557', 0, '+5519971153682', 'aeon@admin.com',
			 '1991-12-24', 'My Week Admin');


INSERT INTO mw_usuario(
	id, version, criado_em, ativo, login, perfil, senha, cliente_id)
	VALUES ('5f1b9933-e14d-4ffd-89e2-ff1344a65889', 0, CURRENT_DATE, 'true', 'aeon@admin.com',
			'ADMINISTRADOR', '$2y$12$BG5gn4NfOALOSQ8nsZQyker4dg5eYJCnxNVgGEpNeY0BmseasICHC',
			'0be8e982-b356-4a68-bc05-8b9fc7bb1557');