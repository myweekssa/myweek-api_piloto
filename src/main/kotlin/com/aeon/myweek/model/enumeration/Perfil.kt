package com.aeon.myweek.model.enumeration

enum class Perfil(val tipo: String){
    ADM("ADMINISTRADOR"),
    ORG("ORGANIZADOR"),
    CLI("CLIENTE")
}