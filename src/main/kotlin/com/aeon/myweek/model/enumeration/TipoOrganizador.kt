package com.aeon.myweek.model.enumeration

enum class TipoOrganizador(val tipo: String){
    PF("PESSOA FISICA"),
    PJ("PESSOA JURIDICA")
}