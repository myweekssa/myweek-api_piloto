package com.aeon.myweek.model.enumeration

enum class StatusEvento(val status: String) {
    AGD("AGENDADO"),
    ADM("ANDAMENTO"),
    FNZ("FINALIZADO"),
    CNL("CANCELADO")
}