package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*

@Entity
@Table(name ="mw_cliente")
open class Cliente(

        @Column(name="nome", nullable=false)
        var nome: String,

        @Column(name="dtNascimento", nullable=false)
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        @Temporal(TemporalType.DATE)
        var dtNascimento: Date

): Pessoa(){

        @ManyToMany(fetch= FetchType.EAGER, targetEntity = Genero::class)
        @JoinColumn(name = "genero_id")
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        var genero: MutableList<Genero>? = null;
}
