package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email

@Entity
@Table(name="mw_usuario")
open class UsuarioAcesso(
        @Column(name="login", length=250, nullable=false, unique=true)
        @Email(message="Informe um email válido")
        var login: String,

        @Column(name="senha", length=250, nullable=false)
        var senha: String,

        @Column(name="perfil", length = 15, nullable=false)
        var perfil: String
    ): BasicModel(){

    @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
    @JoinColumn(name = "cliente_id", nullable = true)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var cliente: Cliente? = null;

    @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
    @JoinColumn(name = "organizador_id", nullable = true)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var organizador: Organizador? = null;

    @Column(name="ativo", nullable = false)
    var habilitado: Boolean = true;

    @Column(name="criadoEm", nullable=false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    var criadoEm: Date = now();
}