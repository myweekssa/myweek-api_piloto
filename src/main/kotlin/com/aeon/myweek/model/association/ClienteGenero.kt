package com.aeon.myweek.model.association

import java.util.*
import javax.persistence.*

@Entity
@IdClass(ClienteGeneroId::class)
@Table(name = "mw_cliente_genero")
class ClienteGenero (
        @Id
        @Column(name="cliente_id")
        var cliente_id : UUID,
        @Id
        @Column(name="genero_id")
        var genero_id : UUID
){}