package com.aeon.myweek.model.association

import java.io.Serializable
import java.util.*

class ClienteGeneroId : Serializable{
    var cliente_id: UUID? = null
    var genero_id: UUID? = null
}