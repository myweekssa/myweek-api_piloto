package com.aeon.myweek.model.association

import java.io.Serializable
import java.util.*

class EventoGeneroId: Serializable {
    val evento_id: UUID? = null
    val genero_id: UUID? = null
}