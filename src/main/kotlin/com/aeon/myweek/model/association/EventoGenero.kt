package com.aeon.myweek.model.association

import java.util.*
import javax.persistence.*

@Entity
@IdClass(EventoGeneroId::class)
@Table(name = "mw_evento_genero")
class EventoGenero (
        @Id
        @Column(name="evento_id")
        var evento_id : UUID,
        @Id
        @Column(name="genero_id")
        var genero_id : UUID
){}