package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.io.Serializable
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*
import javax.persistence.Id
import javax.persistence.Inheritance
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
@Inheritance
@DynamicUpdate
@DynamicInsert
abstract class BasicModel: Serializable{

    @Id
    var id: UUID = UUID.randomUUID();

    @Version
    @JsonIgnore
    var version: Int = 0;

    fun now(): Date {
        val zdt: ZonedDateTime = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Brazil/East"))
        val date: Date = Date.from(zdt.toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant())
        return date
    }
}