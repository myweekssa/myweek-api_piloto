package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.*

@Entity
@Table(name ="mw_organizador")
open class Organizador(

        @Column(name="razaoSocial", length = 250, nullable = false)
        val razaoSocial: String,

        @Column(name="nomeFantasia", length = 250, nullable = false)
        val nomeFantasia: String,

        @Column(name="documento", length = 15, nullable = false)
        val documento: String,

        @Column(name = "tipoOrganizador", length = 15, nullable = false)
        val tipoOrganizador: String

): Pessoa(){
        @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
        @JoinColumn(name = "endereco_id")
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        var endereco: Endereco? = null;
}