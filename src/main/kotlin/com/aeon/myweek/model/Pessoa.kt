package com.aeon.myweek.model

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.validation.constraints.Email

@MappedSuperclass
abstract class Pessoa: BasicModel(){
    @Email(message = "Informe um email válido")
    @Column(name="email", length = 255, nullable = false, unique = true)
    var email: String = "";

    @Column(name="celular", length = 15)
    var celular: String = "";
}