package com.aeon.myweek.model

import com.aeon.myweek.model.enumeration.Estado
import javax.persistence.*
import javax.validation.constraints.PositiveOrZero

@Entity
@Table(name="mw_endereco")
open class Endereco(
        @Column(name="cep", length=10, nullable=false)
        var cep: String,

        @Column(name="logradouro", length=100, nullable=false)
        var logradouro: String,

        @Column(name="numero" ,length=5, nullable=false)
        @PositiveOrZero
        var numero: String,

        @Column(name="complemento", length=50)
        var complemento: String,

        @Column(name="bairro", length=50, nullable=false)
        var bairro: String,

        @Column(name="estado",length=3, nullable=false)
        @Enumerated(EnumType.STRING)
        var estado: Estado,

        @Column(name="cidade", length=50, nullable=false)
        var cidade: String
): BasicModel()