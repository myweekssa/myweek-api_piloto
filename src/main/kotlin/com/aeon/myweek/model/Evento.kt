package com.aeon.myweek.model

import com.aeon.myweek.model.enumeration.Estado
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@Entity
@Table(name ="mw_evento")
open class Evento(

        @Column(name="nome", nullable=false)
        @NotEmpty
        val nome: String,

        @Column(name="valor", nullable=false)
        @NotEmpty
        val valor: String,

        @Column(name="dtEvento", nullable=false)
        @NotBlank
        @DateTimeFormat(pattern = "dd/MM/yyyy")
        @Temporal(TemporalType.DATE)
        val dtEvento: Date,

        @Column(name="atracoes", nullable=false)
        @NotEmpty
        val atracoes: String,

        @Column(name = "status", length = 10, nullable = false)
        @NotEmpty
        val status: String

): BasicModel() {
    @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
    @JoinColumn(name = "organizador_id", nullable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var organizador: Organizador = Organizador("", "", "", "");

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, targetEntity = Galeria::class)
    @JoinColumn(name = "galeria_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Lob
    var galeria: Set<Galeria> = setOf(Galeria(""));

    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER, targetEntity = Genero::class)
    @JoinColumn(name = "genero_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var genero: Set<Genero> = setOf(Genero(""));

    @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
    @JoinColumn(name = "endereco_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var endereco: Endereco = Endereco("", "", "", "", "", Estado.BA, "")

    @Column(name="criadoEm", nullable=false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    var criadoEm: Date = now();
}