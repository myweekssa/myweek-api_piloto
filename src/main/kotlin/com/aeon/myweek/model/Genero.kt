package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*

@Entity
@Table(name ="mw_genero")
open class Genero(
        @Column(name="nome", nullable=false)
        val nome: String
): BasicModel() {

    @OneToOne(cascade = [(CascadeType.ALL)], fetch= FetchType.EAGER)
    @JoinColumn(name = "categoria_id")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var categoria: Categoria = Categoria("");

    @Column(name="criadoEm", nullable=false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    var criadoEm: Date = now();

}