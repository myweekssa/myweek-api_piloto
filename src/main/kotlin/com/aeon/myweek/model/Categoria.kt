package com.aeon.myweek.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.format.annotation.DateTimeFormat
import java.util.*
import javax.persistence.*

@Entity
@Table(name ="mw_categoria")
open class Categoria(
        @Column(name="nome", nullable=false)
        val nome: String
): BasicModel(){
    @Column(name="criadoEm", nullable=false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    @JsonIgnore
    var criadoEm: Date = now()
}