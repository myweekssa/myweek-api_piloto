package com.aeon.myweek.model

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Lob
import javax.persistence.Table
import javax.validation.constraints.NotEmpty

@Entity
@Table(name ="mw_galeria")
open class Galeria(
        @Column(name="url", nullable=false)
        @Lob
        val url: String
): BasicModel()