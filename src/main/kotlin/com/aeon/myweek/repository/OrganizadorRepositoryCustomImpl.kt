package com.aeon.myweek.repository

import com.aeon.myweek.model.Organizador
import org.apache.commons.lang3.StringUtils
import org.hibernate.Criteria
import org.hibernate.Session
import org.hibernate.criterion.MatchMode
import org.hibernate.criterion.Order
import org.hibernate.criterion.Restrictions
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

class OrganizadorRepositoryCustomImpl: OrganizadorRepositoryCustom {

    @PersistenceContext
    private lateinit var manager: EntityManager;

    override fun buscarClientePorNomeFantasiaOuDocumento(param: String): List<Organizador> {
        val session = manager.delegate as Session;
        val criteria: Criteria = session.createCriteria(Organizador::class.java);

        if (param != null) {
            if (StringUtils.isNumeric(param)){
                criteria.add(Restrictions.eq("documento", param));
            }else{
                criteria.add(Restrictions.ilike("nomeFantasia", param, MatchMode.ANYWHERE));
            }
        }
        criteria.addOrder(Order.asc("nomeFantasia"));
        val list: ArrayList<Organizador> = criteria.list() as ArrayList<Organizador>;
        return list;
    }
}