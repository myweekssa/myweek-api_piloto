package com.aeon.myweek.repository

import com.aeon.myweek.model.Cliente

interface ClienteRepositoryCustom {
    fun buscarClientePorNomeOuDocumento(param: String): List<Cliente>;
}