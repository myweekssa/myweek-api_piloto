package com.aeon.myweek.repository

import com.aeon.myweek.model.Evento
import com.aeon.myweek.vo.EventoClienteVO
import org.springframework.data.repository.query.Param

interface EventoRepositoryCustom {
    fun buscarEventoByFiltro(param: String): Evento?;

    fun buscarEventoByCliente(@Param("email")email: String,
                              @Param("city")city: String): List<EventoClienteVO>;

}