package com.aeon.myweek.repository

import com.aeon.myweek.model.Organizador
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface OrganizadorRepository: JpaRepository<Organizador, UUID>, OrganizadorRepositoryCustom {
    fun findByEmail(email: String): Organizador;
}