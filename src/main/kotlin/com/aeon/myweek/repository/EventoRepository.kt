package com.aeon.myweek.repository

import com.aeon.myweek.model.Evento
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EventoRepository : JpaRepository<Evento, UUID>, EventoRepositoryCustom {
}