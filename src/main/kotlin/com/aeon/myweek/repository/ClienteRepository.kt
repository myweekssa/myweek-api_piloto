package com.aeon.myweek.repository

import com.aeon.myweek.model.Cliente
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ClienteRepository: JpaRepository<Cliente, UUID>, ClienteRepositoryCustom {
    fun findByEmail(email: String): Cliente?;
}