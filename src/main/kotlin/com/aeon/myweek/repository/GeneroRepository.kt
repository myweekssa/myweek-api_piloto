package com.aeon.myweek.repository

import com.aeon.myweek.model.Genero
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface GeneroRepository: JpaRepository<Genero, UUID> {
    fun findByNome(nome: String): Genero;
}