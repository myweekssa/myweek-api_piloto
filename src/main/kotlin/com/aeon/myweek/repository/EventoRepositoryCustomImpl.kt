package com.aeon.myweek.repository

import com.aeon.myweek.model.Evento
import com.aeon.myweek.vo.EventoClienteVO
import org.apache.commons.lang3.StringUtils
import org.hibernate.Criteria
import org.hibernate.Session
import org.hibernate.criterion.MatchMode
import org.hibernate.criterion.Order
import org.hibernate.criterion.Restrictions
import org.hibernate.type.StringType
import org.hibernate.type.UUIDCharType
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query
import kotlin.collections.ArrayList


class EventoRepositoryCustomImpl: EventoRepositoryCustom {

    @PersistenceContext
    private lateinit var manager: EntityManager;

    override fun buscarEventoByFiltro(param: String): Evento? {
        val session = manager.delegate as Session;
        val criteria: Criteria = session.createCriteria(Evento::class.java);

        if (param != null) {
            if (StringUtils.isNumeric(param)){
                criteria.add(Restrictions.eq("dtEvento", param));
            }else{
                criteria.add(Restrictions.ilike("nome", param, MatchMode.ANYWHERE));
            }
        }
        criteria.addOrder(Order.asc("nome"));

        val list: ArrayList<Evento> = criteria.list() as ArrayList<Evento>;
        return if (list.isNotEmpty())list.get(0) else null;
    }

    override fun buscarEventoByCliente(email: String, city: String): List<EventoClienteVO> {
        val session = manager.delegate as Session;

        val sql: String = "SELECT cli.id AS cliente_id, " +
                "cli.nome AS cliente_nome, " +
                "cli.email AS cliente_email, " +
                "gen.nome AS genero_nome, " +
                "eve.id AS evento_id, " +
                "eve.nome AS evento_nome, " +
                "TO_CHAR(eve.dt_evento, 'DD/MM/YYYY') AS evento_data, " +
                "eve.status AS evento_status " +
                "FROM mw_cliente_genero as cgr " +
                "INNER JOIN mw_cliente AS cli ON cli.id = cgr.cliente_id " +
                "INNER JOIN mw_genero AS gen ON gen.id = cgr.genero_id " +
                "INNER JOIN mw_evento_genero AS egr ON egr.genero_id = gen.id " +
                "INNER JOIN mw_evento AS eve ON eve.id = egr.evento_id " +
                "INNER JOIN mw_endereco AS loc ON loc.id = eve.endereco_id "+
                "WHERE CLI.EMAIL = :email " +
                "AND LOC.cidade = :city "+
                "ORDER BY evento_status ASC"

        val query: Query = session.createNativeQuery(sql)
                .addScalar("cliente_id", UUIDCharType.INSTANCE)
                .addScalar("cliente_nome", StringType.INSTANCE)
                .addScalar("cliente_email", StringType.INSTANCE)
                .addScalar("genero_nome", StringType.INSTANCE)
                .addScalar("evento_id", UUIDCharType.INSTANCE)
                .addScalar("evento_nome", StringType.INSTANCE)
                .addScalar("evento_data", StringType.INSTANCE)
                .addScalar("evento_status", StringType.INSTANCE)
                .setParameter("email", email)
                .setParameter("city", city)
        val linhas: MutableList<Array<Object>> = query.resultList as MutableList<Array<Object>>
        val lista: MutableList<EventoClienteVO> = ArrayList()
        for (l in linhas) {
            val vo = EventoClienteVO()
            vo.clienteId = UUID.fromString(l[0].toString())
            vo.clienteNome = l[1].toString()
            vo.clienteEmail = l[2].toString()
            vo.generoNome = l[3].toString()
            vo.eventoId = UUID.fromString(l[4].toString())
            vo.eventoNome = l[5].toString()
            val formatter: DateFormat = SimpleDateFormat("dd/MM/yyyy")
            vo.eventoData = java.sql.Date((formatter.parse(l[6].toString()) as Date).time)
            vo.eventoStatus = l[7].toString()
            lista.add(vo)
        }
        return lista
    }

}