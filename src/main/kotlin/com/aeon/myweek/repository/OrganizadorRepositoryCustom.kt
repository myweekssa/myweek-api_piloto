package com.aeon.myweek.repository

import com.aeon.myweek.model.Organizador

interface OrganizadorRepositoryCustom{
    fun buscarClientePorNomeFantasiaOuDocumento(param: String): List<Organizador>;
}