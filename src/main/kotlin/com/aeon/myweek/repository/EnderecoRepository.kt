package com.aeon.myweek.repository

import com.aeon.myweek.model.Endereco
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EnderecoRepository: JpaRepository<Endereco, UUID> {
    fun findByCep(cep: String): Endereco?;
}