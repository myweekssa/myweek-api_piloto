package com.aeon.myweek.repository

import com.aeon.myweek.model.Cliente
import org.apache.commons.lang3.StringUtils
import org.hibernate.Criteria
import org.hibernate.Session
import org.hibernate.criterion.MatchMode
import org.hibernate.criterion.Order
import org.hibernate.criterion.Restrictions
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

class ClienteRepositoryCustomImpl : ClienteRepositoryCustom {

    @PersistenceContext
    private lateinit var manager: EntityManager;

    override fun buscarClientePorNomeOuDocumento(param: String): List<Cliente> {
        val session = manager.delegate as Session;
        val criteria: Criteria = session.createCriteria(Cliente::class.java);

        if (!param.isBlank()) {
            if (StringUtils.isNumeric(param)){
                criteria.add(Restrictions.eq("documento", param));
            }else{
                criteria.add(Restrictions.ilike("nome", param, MatchMode.ANYWHERE));
            }
        }
        criteria.addOrder(Order.asc("nome"));
        @Suppress("UNCHECKED_CAST") val list: MutableList<Cliente> = criteria.list() as MutableList<Cliente>;
        return list;
    }

}