package com.aeon.myweek.repository

import com.aeon.myweek.model.association.ClienteGenero
import com.aeon.myweek.model.association.ClienteGeneroId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ClienteGeneroRepository: JpaRepository<ClienteGenero, ClienteGeneroId> {
}