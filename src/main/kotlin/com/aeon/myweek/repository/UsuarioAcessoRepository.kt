package com.aeon.myweek.repository

import com.aeon.myweek.model.UsuarioAcesso
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UsuarioAcessoRepository: JpaRepository<UsuarioAcesso, UUID> {
    fun findByLogin(login: String): UsuarioAcesso;
}