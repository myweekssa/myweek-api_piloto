package com.aeon.myweek.vo

import java.util.*

class EventoClienteVO (){
    var clienteId: UUID? = null
    var clienteNome: String? = null
    var clienteEmail: String? = null
    var generoNome: String? = null
    var eventoId: UUID? = null
    var eventoNome: String? = null
    var eventoData: Date? = null
    var eventoStatus: String? = null
}