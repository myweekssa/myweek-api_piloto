package com.aeon.myweek.vo

import com.fasterxml.jackson.annotation.JsonProperty

class ClimaDetalhadoVO {

    var current: Current? = null


    class Current {
        @JsonProperty("temp_c")
        val temp_c: Long? = null

        var condition: Condition? = null

        class Condition{
            @JsonProperty("icon")
            val icon: String? = null
        }

    }


}