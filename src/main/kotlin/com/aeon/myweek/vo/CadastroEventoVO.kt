package com.aeon.myweek.vo

import com.aeon.myweek.model.Galeria
import com.aeon.myweek.model.Genero
import com.aeon.myweek.model.enumeration.Estado
import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class CadastroEventoVO(

        val eveId: String,

        @field:NotBlank(message = "Nome é um campo obrigatorio")
        val nome: String,

        @field:NotBlank(message = "Valor é um campo obrigatorio")
        val valor: String,

        @field:NotNull(message = "Data é um campo obrigatorio")
        val dtEvento: Date,

        @field:NotBlank(message = "Atrações é um campo obrigatorio")
        val atracoes: String,

        @field:NotBlank(message = "Documento é um campo obrigatorio")
        val docOrganizacao: String,

        val fotos: List<Galeria>,

        @field:NotBlank(message = "Cep é um campo obrigatorio")
        val cep: String,

        @field:NotBlank(message = "Logradouro é um campo obrigatorio")
        val logradouro: String,

        val numero: String,
        val complemento: String,

        @field:NotBlank(message = "Bairro é um campo obrigatorio")
        val bairro: String,

        @field:NotBlank(message = "Estado é um campo obrigatorio")
        @get:Size(min=2, max=2, message = "Estado deve ter apenas 2 caracteres. EX: BA, SP, RJ")
        val estado: Estado,

        @field:NotBlank(message = "Cidade é um campo obrigatorio")
        val cidade: String,

        val genero: List<Genero>
){}