package com.aeon.myweek.vo

import com.fasterxml.jackson.annotation.JsonProperty

class ClienteIpAddrVO {
    var status: String? = null
    var data: Data? = null

    class Data {
        @JsonProperty("city_name")
        var cityName: String? = null

        @JsonProperty("latitude")
        var latitude: String? = null

        @JsonProperty("longitude")
        var longitude: String? = null
    }
}