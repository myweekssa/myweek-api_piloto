package com.aeon.myweek.vo

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

class ResponseApiVO<T>{
    var timestamp: Date = now()
    var status: String = ""
    var error: String = ""
    var dados: T ? = null
    var errors: MutableList<String> = arrayListOf()

    fun now(): Date {
        val zdt: ZonedDateTime = ZonedDateTime.ofInstant(Instant.now(), ZoneId.of("Brazil/East"))
        val date: Date = Date.from(zdt.toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant())
        return date
    }
}