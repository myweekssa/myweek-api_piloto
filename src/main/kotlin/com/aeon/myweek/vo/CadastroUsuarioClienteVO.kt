package com.aeon.myweek.vo

import java.util.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class CadastroUsuarioClienteVO(

        val cliId: String,
        @field:NotBlank(message = "Nome é um campo obrigatorio")
        val nome: String,
        @field:NotBlank(message = "E-mail é um campo obrigatorio")
        val email: String,
        @field:NotBlank(message = "Celular é um campo obrigatorio")
        val celular: String,
        @field:NotNull(message = "Data Nascimento é um campo obrigatorio")
        val dtNascimento: Date,

        val senha: String,
        val novaSenha: String,

        val genero: MutableList<String>
){}
