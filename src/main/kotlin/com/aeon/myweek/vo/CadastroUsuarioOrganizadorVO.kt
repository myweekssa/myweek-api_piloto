package com.aeon.myweek.vo

import com.aeon.myweek.model.enumeration.Estado
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class CadastroUsuarioOrganizadorVO(

        val empId: String,

        @field:NotBlank(message = "Razão Social é um campo obrigatorio")
        val razaoSocial: String,

        @field:NotBlank(message = "Nome Fantasia é um campo obrigatorio")
        val nomeFantasia: String,

        @field:NotBlank(message = "Documento é um campo obrigatorio")
        val documento: String,

        @field:NotBlank(message = "E-mail é um campo obrigatorio")
        val email: String,

        @field:NotBlank(message = "Celular é um campo obrigatorio")
        val celular: String,

        val observacao: String,

        val senha: String,

        @field:NotBlank(message = "Cep é um campo obrigatorio")
        val cep: String,

        @field:NotBlank(message = "Logradouro é um campo obrigatorio")
        val logradouro: String,

        val numero: String,
        val complemento: String,

        @field:NotBlank(message = "Bairro é um campo obrigatorio")
        val bairro: String,

        @field:NotBlank(message = "Estado é um campo obrigatorio")
        @get:Size(min=2, max=2, message = "Estado deve ter apenas 2 caracteres. EX: BA, SP, RJ")
        val estado: Estado,

        @field:NotBlank(message = "Cidade é um campo obrigatorio")
        val cidade: String
)