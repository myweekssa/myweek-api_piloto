package com.aeon.myweek.security

import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.LogoutHandler
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class LogoutHandlerImpl : LogoutHandler {
    override fun logout(p0: HttpServletRequest?, response: HttpServletResponse, p2: Authentication?) {
        response.status = HttpServletResponse.SC_OK

    }
}