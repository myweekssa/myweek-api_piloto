package com.aeon.myweek.security

import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.repository.UsuarioAcessoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class UserDetailServiceImpl @Autowired constructor(
    private var usuarioAcessoRepository: UsuarioAcessoRepository
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {
        val usuario = usuarioAcessoRepository.findByLogin(username);
        return User(usuario.login, usuario.senha, getPermissoes(usuario));
    }

    fun getPermissoes(usuario: UsuarioAcesso) : Collection<SimpleGrantedAuthority> {
        return setOf<SimpleGrantedAuthority>(SimpleGrantedAuthority(usuario.perfil));
    }


}