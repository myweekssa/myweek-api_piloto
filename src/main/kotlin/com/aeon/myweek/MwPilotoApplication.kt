package com.aeon.myweek

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MwPilotoApplication

fun main(args: Array<String>) {
	runApplication<MwPilotoApplication>(*args)
}
