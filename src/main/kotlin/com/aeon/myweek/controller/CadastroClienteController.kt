package com.aeon.myweek.controller

import com.aeon.myweek.business.CadastroClienteService
import com.aeon.myweek.business.ClienteService
import com.aeon.myweek.model.Cliente
import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.vo.CadastroUsuarioClienteVO
import com.aeon.myweek.vo.ResponseApiVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping(value = ["/api/cadastro/cliente"])
class CadastroClienteController @Autowired constructor(
        private var cadClienteService: CadastroClienteService,
        private var clienteService: ClienteService
){

    @RequestMapping(value = ["/new"], method = [RequestMethod.POST])
    fun addCliente(@Valid @RequestBody cliVO: CadastroUsuarioClienteVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<UsuarioAcesso>>{

        var response: ResponseApiVO<UsuarioAcesso> = ResponseApiVO<UsuarioAcesso>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var usuario: UsuarioAcesso? = cadClienteService.efetuarCadastroCliente(cliVO)
        if (usuario == null){
            validationReturn(response)
            response.dados = usuario
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = usuario
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET])
    fun listarCliente(): ResponseEntity<ResponseApiVO<List<Cliente>>> {

        var response: ResponseApiVO<List<Cliente>> = ResponseApiVO<List<Cliente>>()
        val cliente: List<Cliente> = clienteService.buscarTodosCliente();

        if (cliente.isEmpty()){
            validationReturn(response)
            response.dados = cliente
            return ResponseEntity.ok(response);
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = cliente
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/listBy"], method = [RequestMethod.GET])
    fun listarClienteFiltro(@RequestParam("param") param: String):
            ResponseEntity<ResponseApiVO<List<Cliente>>> {

        var response: ResponseApiVO<List<Cliente>> = ResponseApiVO<List<Cliente>>()

        val cliente: List<Cliente> = clienteService.buscarClienteByFiltro(param);
        if (cliente.isEmpty()){
            validationReturn(response)
            response.dados = cliente
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = cliente
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/update"], method = [RequestMethod.PUT])
    fun alterarClienteById(@Valid @RequestBody cliVO: CadastroUsuarioClienteVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<Cliente>>{

        var response: ResponseApiVO<Cliente> = ResponseApiVO<Cliente>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var cli : Cliente? = clienteService.atualizarCliente(cliVO)
        if (cli == null){
            validationReturn(response)
            response.dados = cli
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = cli
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/remove"], method = [RequestMethod.DELETE])
    fun removeClienteById(@RequestParam("email")email: String):
            ResponseEntity<Any>{

        val cliente: List<Cliente> = clienteService.buscarClienteByFiltro(email);
        var response: ResponseApiVO<String> = ResponseApiVO()

        if (cliente.isEmpty()){
            validationReturn(response)
            response.dados = "Não encontrado"
            return ResponseEntity.ok(response)
        }

        cadClienteService.deletarCadastroCliente(email)

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = "Excluido com Sucesso"
        return ResponseEntity.ok(response);
    }

    private fun validationParam(result: BindingResult, response: ResponseApiVO<*>){
            response.status = HttpStatus.BAD_REQUEST.value().toString()
            response.error = HttpStatus.BAD_REQUEST.name
            result.allErrors.forEach { error -> error.defaultMessage?.let { response.errors?.add(it) }}
    }

    private fun validationReturn(response: ResponseApiVO<*>){
        response.status = HttpStatus.NOT_FOUND.value().toString()
        response.error = HttpStatus.NOT_FOUND.name
    }
}