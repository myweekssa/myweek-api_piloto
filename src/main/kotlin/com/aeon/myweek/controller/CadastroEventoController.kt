package com.aeon.myweek.controller

import com.aeon.myweek.business.EventoService
import com.aeon.myweek.model.Evento
import com.aeon.myweek.vo.CadastroEventoVO
import com.aeon.myweek.vo.ResponseApiVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(value = ["/api/cadastro/evento"])
class CadastroEventoController @Autowired constructor(
    private var eventoService: EventoService
){

    @RequestMapping(value = ["/new"], method = [RequestMethod.POST])
    fun addEvento(@Valid @RequestBody eventoVO: CadastroEventoVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<Evento>>{

        var response: ResponseApiVO<Evento> = ResponseApiVO<Evento>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var evento: Evento? = eventoService.salvarEvento(eventoVO)
        if (evento == null){
            validationReturn(response)
            response.dados = evento
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = evento
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET])
    fun listarEvento(): ResponseEntity<ResponseApiVO<List<Evento>>> {

        var response: ResponseApiVO<List<Evento>> = ResponseApiVO<List<Evento>>()
        val evento: List<Evento> = eventoService.buscarTodosEventos()

        if (evento.isEmpty()){
            validationReturn(response)
            response.dados = evento
            return ResponseEntity.ok(response);
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = evento
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/listBy"], method = [RequestMethod.GET])
    fun listarEventoFiltro(@RequestParam("param") param: String):
            ResponseEntity<ResponseApiVO<Evento>> {

        var response: ResponseApiVO<Evento> = ResponseApiVO<Evento>()

        val evento: Evento? = eventoService.buscarEventoByFiltro(param);
        if (evento == null){
            validationReturn(response)
            response.dados = evento
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = evento
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/update"], method = [RequestMethod.PUT])
    fun alterarEvento(@Valid @RequestBody eventoVO: CadastroEventoVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<Evento>>{

        var response: ResponseApiVO<Evento> = ResponseApiVO<Evento>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var eve : Evento? = eventoService.atualizarEvento(eventoVO)
        if (eve == null){
            validationReturn(response)
            response.dados = eve
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = eve
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/remove"], method = [RequestMethod.DELETE])
    fun removeEvento(@RequestParam("param") param: String):
            ResponseEntity<Any>{

        val eve: Evento? = eventoService.buscarEventoByFiltro(param);
        var response: ResponseApiVO<String> = ResponseApiVO()

        if (eve == null){
            validationReturn(response)
            response.dados = "Não encontrado"
            return ResponseEntity.ok(response)
        }

        eventoService.deletarEvento(eve)

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = "Excluido com Sucesso"
        return ResponseEntity.ok(response);
    }

    private fun validationParam(result: BindingResult, response: ResponseApiVO<*>){
        response.status = HttpStatus.BAD_REQUEST.value().toString()
        response.error = HttpStatus.BAD_REQUEST.name
        result.allErrors.forEach { error -> error.defaultMessage?.let { response.errors?.add(it) }}
    }

    private fun validationReturn(response: ResponseApiVO<*>){
        response.status = HttpStatus.NOT_FOUND.value().toString()
        response.error = HttpStatus.NOT_FOUND.name
    }
}