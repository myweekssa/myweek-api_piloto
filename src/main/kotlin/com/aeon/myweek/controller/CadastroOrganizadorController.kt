package com.aeon.myweek.controller

import com.aeon.myweek.business.CadastroOrganizadorService
import com.aeon.myweek.business.OrganizadorService
import com.aeon.myweek.model.Organizador
import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.vo.CadastroUsuarioOrganizadorVO
import com.aeon.myweek.vo.ResponseApiVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping(value = ["/api/cadastro/organizador"])
class CadastroOrganizadorController @Autowired constructor(

        private  var cadOrganizadorService: CadastroOrganizadorService,
        private  var organizadorService: OrganizadorService
){

    @RequestMapping(value = ["/new"], method = [RequestMethod.POST])
    fun addOrganizador(@Valid @RequestBody orgVO: CadastroUsuarioOrganizadorVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<UsuarioAcesso>> {

        var response: ResponseApiVO<UsuarioAcesso> = ResponseApiVO<UsuarioAcesso>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var usuario: UsuarioAcesso? = cadOrganizadorService.efetuarCadastroOrganizador(orgVO)
        if (usuario == null){
            validationReturn(response)
            response.dados = usuario
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = usuario
        return ResponseEntity.ok(response)
    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET])
    fun listarOrganizador(): ResponseEntity<ResponseApiVO<List<Organizador>>> {

        var response: ResponseApiVO<List<Organizador>> = ResponseApiVO<List<Organizador>>()
        val organizador: List<Organizador> = organizadorService.buscarTodosOrganizador();

        if (organizador.isEmpty()){
            validationReturn(response)
            response.dados = organizador
            return ResponseEntity.ok(response);
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = organizador
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/listBy"], method = [RequestMethod.GET])
    fun listarOrganizadorFiltro(@PathVariable("param") param: String):
            ResponseEntity<ResponseApiVO<List<Organizador>>>{

        var response: ResponseApiVO<List<Organizador>> = ResponseApiVO<List<Organizador>>()

        val organizador: List<Organizador> = organizadorService.buscarOrganizadorByFiltro(param);
        if (organizador.isEmpty()){
            validationReturn(response)
            response.dados = organizador
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = organizador
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/update"], method = [RequestMethod.PUT])
    fun alterarOrganizadorById(@Valid @RequestBody orgVO: CadastroUsuarioOrganizadorVO, result: BindingResult):
            ResponseEntity<ResponseApiVO<Organizador>>{

        var response: ResponseApiVO<Organizador> = ResponseApiVO<Organizador>()
        if(result.hasErrors()){
            validationParam(result, response)
            return ResponseEntity.badRequest().body(response)
        }

        var org : Organizador? = organizadorService.atualizarOrganizador(orgVO)
        if (org == null){
            validationReturn(response)
            response.dados = org
            return ResponseEntity.ok(response)
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = org
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = ["/remove"], method = [RequestMethod.DELETE])
    fun removeOrganizadorById(@PathVariable("param") param: String):
            ResponseEntity<Any>{

        val orgs: List<Organizador> = organizadorService.buscarOrganizadorByFiltro(param);
        var response: ResponseApiVO<String> = ResponseApiVO()

        if (orgs.isEmpty()){
            validationReturn(response)
            response.dados = "Não encontrado"
            return ResponseEntity.ok(response)
        }

        organizadorService.deletarOrganizador(orgs.get(0))

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = "Excluido com Sucesso"
        return ResponseEntity.ok(response);
    }

    private fun validationParam(result: BindingResult, response: ResponseApiVO<*>){
        response.status = HttpStatus.BAD_REQUEST.value().toString()
        response.error = HttpStatus.BAD_REQUEST.name
        result.allErrors.forEach { error -> error.defaultMessage?.let { response.errors?.add(it) }}
    }

    private fun validationReturn(response: ResponseApiVO<*>){
        response.status = HttpStatus.NOT_FOUND.value().toString()
        response.error = HttpStatus.NOT_FOUND.name
    }
}