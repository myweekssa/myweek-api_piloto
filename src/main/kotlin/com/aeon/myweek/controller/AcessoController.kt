package com.aeon.myweek.controller

import com.aeon.myweek.business.UsuarioAcessoService
import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.vo.AcessoVO
import com.aeon.myweek.vo.ClienteIpAddrVO
import com.aeon.myweek.vo.ClimaDetalhadoVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.net.InetAddress
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@RestController
class AcessoController @Autowired constructor(
        private var usuarioAcessoService: UsuarioAcessoService
) {

    private val IP_URL = "https://ipvigilante.com/json/{ip}"
    private val CLIMA_URL = "http://api.weatherapi.com/v1/current.json?key=3abf483b8c5e4b37ab115700202007&q={q}"

    @RequestMapping(value = ["/weather"], method = [RequestMethod.GET], produces = arrayOf("text/plain"))
    fun clima(request: HttpServletRequest,@RequestParam("cidade") cidade: String) : String{
        val restTemplate = RestTemplate()
        val clima : ClimaDetalhadoVO = restTemplate.getForObject(CLIMA_URL, ClimaDetalhadoVO::class.java, cidade)!!

        return clima.current?.temp_c.toString()+"-"+clima.current?.condition?.icon
    }

    @RequestMapping(value = ["/login"], method = [RequestMethod.GET], produces = arrayOf("application/json"))
    fun dashboard(authentication: Authentication, request: HttpServletRequest): AcessoVO {

        var ipAddress = getClientIpAddr(request)

        if (ipAddress.equals("0:0:0:0:0:0:0:1")) {
            val localip = InetAddress.getLocalHost()
            ipAddress = localip.hostAddress
        }

        val restTemplate = RestTemplate()
        val local: ClienteIpAddrVO = restTemplate.getForObject(IP_URL, ClienteIpAddrVO::class.java, ipAddress)!!

        var usuario: UsuarioAcesso = usuarioAcessoService.findByLogin(authentication.name)
        var acesso: AcessoVO = AcessoVO()
        if (usuario != null){
            if (usuario.cliente != null){
                acesso.nome = usuario.cliente!!.nome
                acesso.email = usuario.cliente!!.email
                acesso.local = local.data?.cityName!!
            }
            if (usuario.organizador != null) {
                acesso.nome = usuario.organizador!!.nomeFantasia
                acesso.email = usuario.organizador!!.email
                acesso.local = local.data?.cityName!!
            }
        }
        return acesso;
    }

    @RequestMapping(value = ["/app/logout"], method = [RequestMethod.GET])
    fun logoutDo(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {

        SecurityContextLogoutHandler().logout(request, response, authentication);
        SecurityContextHolder.clearContext();
        var session = request.getSession(false)

        var cookie = Cookie("JSESSIONID", null)
        cookie.path = "/"
        cookie.maxAge = 0
        response.addCookie(cookie)
        response.status = 200;
    }

    fun getClientIpAddr(request: HttpServletRequest): String? {
        var ip = request.getHeader("X-Forwarded-For")
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("Proxy-Client-IP")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("WL-Proxy-Client-IP")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("HTTP_CLIENT_IP")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.remoteAddr
        }

        return ip
    }
}