package com.aeon.myweek.controller

import com.aeon.myweek.business.CalendarioService
import com.aeon.myweek.vo.EventoClienteVO
import com.aeon.myweek.vo.ResponseApiVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin(origins = arrayOf("http://localhost:4200"))
@RequestMapping(value = ["/api/calendario"])
class CalendarioEventoController @Autowired constructor(
        private var calendarioService: CalendarioService
){

    @RequestMapping(value = ["/allEvents"], method = [RequestMethod.GET])
    fun listarEventosCliente(@RequestParam("email") email: String,
                             @RequestParam("city") city: String):
            ResponseEntity<ResponseApiVO<List<EventoClienteVO>>> {

        var response: ResponseApiVO<List<EventoClienteVO>> = ResponseApiVO<List<EventoClienteVO>>()
        val eventoCliente: List<EventoClienteVO> = calendarioService.buscarEventosCliente(email, city);

        if (eventoCliente.isEmpty()){
            validationReturn(response)
            response.dados = eventoCliente
            return ResponseEntity.ok(response);
        }

        response.status = HttpStatus.OK.value().toString()
        response.error = HttpStatus.OK.name
        response.dados = eventoCliente
        return ResponseEntity.ok(response);
    }

    private fun validationParam(result: BindingResult, response: ResponseApiVO<*>){
        response.status = HttpStatus.BAD_REQUEST.value().toString()
        response.error = HttpStatus.BAD_REQUEST.name
        result.allErrors.forEach { error -> error.defaultMessage?.let { response.errors?.add(it) }}
    }

    private fun validationReturn(response: ResponseApiVO<*>){
        response.status = HttpStatus.NOT_FOUND.value().toString()
        response.error = HttpStatus.NOT_FOUND.name
    }
}