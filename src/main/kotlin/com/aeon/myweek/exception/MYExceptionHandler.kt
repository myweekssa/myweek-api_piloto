package com.aeon.myweek.exception

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.web.bind.annotation.ControllerAdvice


@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
class MYExceptionHandler {

//    @ExceptionHandler(MethodArgumentNotValidException::class)
//    fun handleMethodArgumentNotValidException(error: MethodArgumentNotValidException): ResponseEntity<*>? {
//        return ResponseEntity.badRequest().body()
//    }
}