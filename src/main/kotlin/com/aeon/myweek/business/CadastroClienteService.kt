package com.aeon.myweek.business

import com.aeon.myweek.model.Cliente
import com.aeon.myweek.model.Genero
import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.model.enumeration.Perfil
import com.aeon.myweek.vo.CadastroUsuarioClienteVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CadastroClienteService @Autowired constructor(

        private  var clienteService: ClienteService,
        private  var enderecoService: EnderecoService,
        private  var usuarioAcessoService: UsuarioAcessoService,
        private  var encode: PasswordService
){

    fun efetuarCadastroCliente(clienteVO: CadastroUsuarioClienteVO): UsuarioAcesso{
        val cliente: Cliente? = clienteService.salvarCliente(clienteVO);
        val user: UsuarioAcesso = UsuarioAcesso("", "", "");
        if (cliente != null){
            user.login = cliente.email
            user.senha = encode.encodeSenha().encode(clienteVO.senha)
            user.perfil = Perfil.CLI.tipo
            var lista : List<Cliente> = clienteService.buscarTodosCliente()
            for (cli in lista){
                if (cli.email.equals(cliente.email)){
                    user.cliente = cli;
                    break
                }
            }
        };
        var usuario: UsuarioAcesso =  usuarioAcessoService.salvarUsuarioAcesso(user)
        return usuario
    }

    fun deletarCadastroCliente(email: String){
        var cliente: Cliente? = clienteService.buscarClienteByEmail(email);
        if (cliente != null){
            var generoList: MutableList<Genero>? = cliente.genero
            if (generoList != null && generoList.size > 0){
                clienteService.deletarGenerosCliente(cliente, generoList)
            }
        }

        var usuario: UsuarioAcesso? = usuarioAcessoService.findByLogin(email);
        if(usuario != null){
            usuarioAcessoService.deletarUsuario(usuario.id)
        }
    }

}