package com.aeon.myweek.business

import com.aeon.myweek.repository.EventoRepository
import com.aeon.myweek.vo.EventoClienteVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CalendarioService @Autowired constructor(

        private var repo : EventoRepository
){

    fun buscarEventosCliente(email:String, city:String): List<EventoClienteVO> {
        var lista: MutableList<EventoClienteVO> = ArrayList()
        lista = repo.buscarEventoByCliente(email, city) as MutableList<EventoClienteVO>;
        return lista;
    }
}