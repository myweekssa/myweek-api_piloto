package com.aeon.myweek.business

import com.aeon.myweek.model.Organizador
import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.model.enumeration.Perfil
import com.aeon.myweek.vo.CadastroUsuarioOrganizadorVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CadastroOrganizadorService @Autowired constructor(

        private var organizadorService: OrganizadorService,
        private  var usuarioAcessoService: UsuarioAcessoService,
        private  var encode: PasswordService
){

    fun efetuarCadastroOrganizador(organizadorVO: CadastroUsuarioOrganizadorVO): UsuarioAcesso?{
        try {
            val organizador: Organizador = organizadorService.salvarOrganizador(organizadorVO);
            val user: UsuarioAcesso = UsuarioAcesso(organizador.email,
                    encode.encodeSenha().encode(organizadorVO.senha),
                    Perfil.ORG.tipo);
            user.organizador = organizador;
            return usuarioAcessoService.salvarUsuarioAcesso(user);
        }catch (e: Exception){
            println("Erro: " + e.message);
        }
        return null;
    }
}