package com.aeon.myweek.business

import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class PasswordService {

    @Bean
    fun encodeSenha(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder();
    }

}