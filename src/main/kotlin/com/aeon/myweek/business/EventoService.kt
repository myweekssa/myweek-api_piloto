package com.aeon.myweek.business

import com.aeon.myweek.model.Endereco
import com.aeon.myweek.model.Evento
import com.aeon.myweek.model.Genero
import com.aeon.myweek.model.Organizador
import com.aeon.myweek.model.enumeration.StatusEvento
import com.aeon.myweek.repository.EventoRepository
import com.aeon.myweek.repository.GeneroRepository
import com.aeon.myweek.vo.CadastroEventoVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class EventoService @Autowired constructor(
        private var repo : EventoRepository,
        private var organizadorService: OrganizadorService,
        private var enderecoService: EnderecoService,
        private var generoRepo: GeneroRepository
){

    fun salvarEvento(eventoVO: CadastroEventoVO): Evento{

        val evento = Evento(eventoVO.nome, eventoVO.valor, eventoVO.dtEvento,
                eventoVO.atracoes, StatusEvento.AGD.status);

        var organizador: Organizador =
                organizadorService.buscarOrganizadorByFiltro(eventoVO.docOrganizacao).get(0);
        evento.organizador = organizador;

        evento.galeria = eventoVO.fotos.toSet();

        var generoList = mutableSetOf<Genero>();
        eventoVO.genero.forEach {
            var gen: Genero = generoRepo.findByNome(it.nome);
            generoList.add(gen)
        }
        evento.genero = generoList;

        var endereco: Endereco? = enderecoService.buscarEnderecoByCep(eventoVO.cep);
        if (endereco == null) endereco = Endereco(eventoVO.cep, eventoVO.logradouro, eventoVO.numero,
                eventoVO.complemento, eventoVO.bairro, eventoVO.estado,
                eventoVO.cidade);

        enderecoService.salvarEndereco(endereco);
        evento.endereco = endereco;

        repo.saveAndFlush(evento);
        return evento;
    }

    fun buscarTodosEventos(): List<Evento>{
        return repo.findAll(Sort.by("nome").ascending());
    }

    fun buscarEventoByFiltro(param: String): Evento?{
        return  repo.buscarEventoByFiltro(param);
    }

    fun atualizarEvento(eveVO: CadastroEventoVO): Evento? {
        var eve: Evento? = buscarEventoByFiltro(eveVO.nome)
        return eve
    }

    fun deletarEvento(eve: Evento){

    }

}