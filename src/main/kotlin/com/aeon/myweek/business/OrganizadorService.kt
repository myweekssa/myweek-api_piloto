package com.aeon.myweek.business

import com.aeon.myweek.model.Endereco
import com.aeon.myweek.model.Organizador
import com.aeon.myweek.model.enumeration.TipoOrganizador
import com.aeon.myweek.repository.OrganizadorRepository
import com.aeon.myweek.vo.CadastroUsuarioOrganizadorVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class OrganizadorService @Autowired constructor(
        private  var repo: OrganizadorRepository,
        private  var enderecoService: EnderecoService
){

    fun salvarOrganizador(userOrganizadorVO: CadastroUsuarioOrganizadorVO): Organizador{

        val organizador = Organizador(userOrganizadorVO.razaoSocial, userOrganizadorVO.nomeFantasia, userOrganizadorVO.documento,
                if (userOrganizadorVO.documento.length.equals(11)) TipoOrganizador.PF.tipo else TipoOrganizador.PJ.tipo);
        organizador.email = userOrganizadorVO.email;
        organizador.celular = userOrganizadorVO.celular;

        var endereco = Endereco(userOrganizadorVO.cep, userOrganizadorVO.logradouro, userOrganizadorVO.numero,
                userOrganizadorVO.complemento, userOrganizadorVO.bairro, userOrganizadorVO.estado,
                userOrganizadorVO.cidade);
        enderecoService.salvarEndereco(endereco);
        organizador.endereco = endereco;

        repo.saveAndFlush(organizador);
        return organizador;
    }

    fun buscarOrganizadorByEmail(email: String): Organizador{
        return repo.findByEmail(email);
    }

    fun buscarTodosOrganizador(): List<Organizador>{
        return repo.findAll(Sort.by("nomeFantasia").ascending());
    }

    fun buscarOrganizadorByFiltro(param: String): List<Organizador>{
        return  repo.buscarClientePorNomeFantasiaOuDocumento(param);
    }

    fun atualizarOrganizador(vo: CadastroUsuarioOrganizadorVO): Organizador?{
        var org: Organizador? = buscarOrganizadorByEmail(vo.email)

        return org
    }

    fun deletarOrganizador(org: Organizador){

    }
}