package com.aeon.myweek.business

import com.aeon.myweek.model.UsuarioAcesso
import com.aeon.myweek.repository.UsuarioAcessoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class UsuarioAcessoService @Autowired constructor(

        private  var repo: UsuarioAcessoRepository
){

    fun salvarUsuarioAcesso(user: UsuarioAcesso): UsuarioAcesso{
        return repo.saveAndFlush(user);
    }

    fun findByLogin(login: String): UsuarioAcesso{
        return repo.findByLogin(login);
    }

    fun deletarUsuario(id: UUID){
        repo.deleteById(id)
        repo.flush()
    }


}