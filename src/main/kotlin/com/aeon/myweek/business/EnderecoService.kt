package com.aeon.myweek.business

import com.aeon.myweek.model.Endereco
import com.aeon.myweek.repository.EnderecoRepository
import com.aeon.myweek.vo.CadastroUsuarioOrganizadorVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class EnderecoService @Autowired constructor(

        private  var repo: EnderecoRepository
){

    fun salvarEndereco(endereco: Endereco){
        repo.saveAndFlush(endereco);
    }

    fun buscarEnderecoById(id: String): Endereco {
        return  repo.findById(UUID.fromString(id)) as Endereco;
    }

    fun buscarEnderecoByCep(cep: String): Endereco? {
        return repo.findByCep(cep);
    }

    fun atualizarEndereco(end: Endereco, vo: CadastroUsuarioOrganizadorVO): Endereco?{

        if(end != null){
            end.logradouro = vo.logradouro;
            end.numero = vo.numero;
            end.complemento = vo.complemento;
            end.bairro = vo.bairro;
            end.cidade = vo.cidade;
            end.estado = vo.estado;
            end.cep = vo.cep;
            salvarEndereco(end);
        }
        return end;
    }

    fun excluirEndereco(end: Endereco){
        repo.deleteById(end.id);
        repo.flush();
    }
}