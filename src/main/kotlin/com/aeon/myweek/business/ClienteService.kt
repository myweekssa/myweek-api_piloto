package com.aeon.myweek.business

import com.aeon.myweek.model.Cliente
import com.aeon.myweek.model.Genero
import com.aeon.myweek.model.association.ClienteGeneroId
import com.aeon.myweek.repository.ClienteGeneroRepository
import com.aeon.myweek.repository.ClienteRepository
import com.aeon.myweek.repository.GeneroRepository
import com.aeon.myweek.vo.CadastroUsuarioClienteVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.*

@Service
class ClienteService @Autowired constructor(
        private var repo: ClienteRepository,
        private var enderecoService: EnderecoService,
        private var generoRepo: GeneroRepository,
        private var cliGeneroRepo: ClienteGeneroRepository
) {

    fun salvarCliente(userClienteVO: CadastroUsuarioClienteVO): Cliente? {

        if (userClienteVO.cliId.isBlank()){
            var cliente: Cliente;
            cliente = Cliente(userClienteVO.nome, userClienteVO.dtNascimento);
            cliente.email = userClienteVO.email;
            cliente.celular = userClienteVO.celular;

            cliente.genero = montarGrupoGenero(userClienteVO, cliente);

            repo.saveAndFlush(cliente);
            return cliente;
        }

        return null
    }

    fun atualizarCliente(vo: CadastroUsuarioClienteVO): Cliente?{
        var cli: Cliente? = buscarClienteByEmail(vo.email)
        if (cli != null) {
            cli.nome = vo.nome
            cli.dtNascimento = vo.dtNascimento;
            cli.celular = vo.celular;

            cli.genero = montarGrupoGenero(vo, cli);
            repo.saveAndFlush(cli);
            return cli ;
        };
        return null
    }

    @Suppress("CAST_NEVER_SUCCEEDS")
    fun buscarClienteById(id: String): Cliente{
        return  repo.findById(UUID.fromString(id)) as Cliente;
    }

    fun buscarClienteByEmail(email: String): Cliente? {
        return repo.findByEmail(email);
    }

    fun buscarTodosCliente(): List<Cliente> {
        var lista: List<Cliente> = repo.findAll(Sort.by("nome").ascending());
        return lista;
    }

    fun buscarClienteByFiltro(param: String): List<Cliente> {
        return repo.buscarClientePorNomeOuDocumento(param);
    }

    fun montarGrupoGenero(vo: CadastroUsuarioClienteVO, cliente: Cliente): MutableList<Genero>? {
        var generoList: MutableList<Genero>? = cliente.genero
        if (generoList != null) {
            deletarGenerosCliente(cliente, generoList)
            generoList.clear()
        }
        generoList = arrayListOf()
        for (g in vo.genero){
            var gen : Genero = generoRepo.findByNome(g);
            generoList.add(gen)
        }

        return generoList;
    }

    fun deletarGenerosCliente(cliente: Cliente, generoList: MutableList<Genero>){
        if (generoList.size > 0){
            for (it in generoList) {
                var cliGenId : ClienteGeneroId = ClienteGeneroId()
                cliGenId.cliente_id = cliente.id
                cliGenId.genero_id = it.id
                cliGeneroRepo.deleteById(cliGenId)
                cliGeneroRepo.flush()
            }
        }
    }
}