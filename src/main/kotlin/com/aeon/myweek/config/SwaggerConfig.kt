package com.aeon.myweek.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.service.BasicAuth
import springfox.documentation.service.Contact
import springfox.documentation.service.SecurityReference
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController::class.java))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(listOf(basicAuth()))
                .securityContexts(listOf(securityContext()))
                .apiInfo(this.apiInfo().build())
    }


    private fun apiInfo(): ApiInfoBuilder {
        val apiInfoBuilder = ApiInfoBuilder()
        apiInfoBuilder.title("Api MyWeek")
        apiInfoBuilder.description("MyWeek services API")
        apiInfoBuilder.version("1.0")
        apiInfoBuilder.termsOfServiceUrl("")
        apiInfoBuilder.contact(contato())
        return apiInfoBuilder
    }

    private fun securityContext(): SecurityContext {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .build()
    }

    private fun defaultAuth(): List<SecurityReference> {
        val authorizationScope = AuthorizationScope("global", "accessEverything")
        val authorizationScopes: Array<AuthorizationScope?> = arrayOfNulls<AuthorizationScope>(1)
        authorizationScopes[0] = authorizationScope
        return listOf(SecurityReference("", authorizationScopes))
    }

    private fun basicAuth(): BasicAuth {
        return BasicAuth("basicAuth")
    }

    private fun contato(): Contact {
        return Contact(
                "Acks Eduardo",
                "https://www.myweek.com.br",
                "myweek@gmail.com")
    }
}